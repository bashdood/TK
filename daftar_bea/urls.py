from django.conf.urls import url
from .views import index, list_bea, tambah_paket, daftar_paket_baru, post_paket_baru, post_tambah_paket

app_name = 'daftar-bea'

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftar-paket/', daftar_paket_baru, name='daftar-paket'),
    url(r'^tambah-paket/', tambah_paket, name='tambah-paket'),
    url(r'^post-paket-baru/', post_paket_baru, name='post-paket-baru'),
    url(r'^post-tambah-paket/', post_tambah_paket, name='post-tambah-paket'),
    url(r'^list-bea/', list_bea, name='list-bea'),
]