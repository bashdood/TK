from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from .models import Skema_beasiswa_aktif, Skema_beasiswa, Syarat_beasiswa

# Create your views here.

dummy_donatur = '3543887734'

def index(request):
    response = {}
    return render(request, 'bea_main.html', response)

def daftar_paket_baru(request):
    response = {}
    return render(request, 'daftar_bea.html', response)

def tambah_paket(request):
    response = {}
    return render(request, 'tambah_bea.html', response)

def list_bea(request):
    response = {}
    response['skema_aktif'] = Skema_beasiswa_aktif.objects.raw("SELECT * FROM simbion.skema_beasiswa_aktif;")
    return render(request, 'list_bea.html', response)

@csrf_exempt
def post_paket_baru(request):
    try:
        if request.method == 'POST':
            with connection.cursor() as c:
                c.execute(create_insert_sql(table_name=Skema_beasiswa._meta.db_table, kode=request.POST['kode_paket'],
                                            nama=request.POST['nama_paket'], jenis=request.POST['jenis_paket'],
                                            deskripsi=request.POST['deskripsi'], nomor_identitas_donatur=dummy_donatur))

                c.execute(create_insert_sql(table_name=Syarat_beasiswa._meta.db_table,
                                            kode_beasiswa=request.POST['kode_paket'],
                                            syarat=request.POST['syarat']))
            messages.success(request, 'Paket baru anda telah terdaftar dalam sistem')
    except:
        messages.error(request, 'Tolong isi semua data dengan benar')
        return HttpResponseRedirect('/daftar-bea/daftar-paket')

    return HttpResponseRedirect('/daftar-bea/')

@csrf_exempt
def post_tambah_paket(request):
    try:
        if request.method == 'POST':
            with connection.cursor() as c:
                c.execute(create_insert_sql(table_name=Skema_beasiswa_aktif._meta.db_table,kode_skema_beasiswa=request.POST['kode_paket'],
                      no_urut=request.POST['no_urut'],tgl_mulai_pendaftaran=request.POST['tgl_buka'],
                      tgl_tutup_pendaftaran=request.POST['tgl_tutup'],status='Buka',
                                            periode_penerimaan='2017/2018'))

            messages.success(request, 'Beasiswa baru anda telah terdaftar dalam sistem')

    except:
        messages.error(request, 'Tolong isi semua data dengan benar')
        return HttpResponseRedirect('/daftar-bea/tambah-paket')

    return HttpResponseRedirect('/daftar-bea/')



def create_insert_sql(table_name, **fields):
    sql = 'INSERT INTO %s (' % table_name
    for field in fields:
        sql += '%s, ' % field
    sql = sql[:-2] + ') VALUES (' # Remove trailing comma
    for field in fields:
        sql += '\'%s\', ' % fields[field]
    sql = sql[:-2] + ');'
    return sql



