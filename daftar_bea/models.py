from django.db import models

# Create your models here.

class Skema_beasiswa(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    jenis = models.CharField(max_length=20)
    deskripsi = models.CharField(max_length=50)
    nomor_identitas_donatur = models.CharField(max_length=20)

    class Meta:
        db_table = 'simbion.skema_beasiswa'

class Syarat_beasiswa(models.Model):
    kode_beasiswa = models.IntegerField(primary_key=True)
    syarat = models.CharField(max_length=50, primary_key=True)

    class Meta:
        db_table = 'simbion.syarat_beasiswa'

class Skema_beasiswa_aktif(models.Model):
    kode_skema_beasiswa = models.IntegerField(primary_key=True)
    no_urut = models.IntegerField(primary_key=True)
    tgl_mulai_pendaftaran = models.DateField()
    tgl_tutup_pendaftaran = models.DateField()
    periode_penerimaan = models.CharField(max_length=50)
    status = models.CharField(max_length=20)
    jumlah_pendaftar = models.IntegerField()

    class Meta:
        db_table = 'simbion.skema_beasiswa_aktif'
