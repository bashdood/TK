"""simbion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import daftar_bea.urls as daftar_bea
import login.urls as login
import regis.urls as regis
import pengumuman_beasiswa.urls as pengumuman_beasiswa

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^daftar-bea/', include(daftar_bea, namespace='daftar-bea')),
    url(r'^login/', include(login, namespace='login')),
    url(r'^regis/', include(regis, namespace='regis')),
	url(r'^$', RedirectView.as_view(url='/login/', permanent='true')),
	url(r'^pengumuman/', include(pengumuman_beasiswa, namespace='pengumuman_beasiswa')),
]
