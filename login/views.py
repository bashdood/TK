from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
from regis.models import pengguna
from django.contrib import messages
import os
import json

response = {}
def index(request):
    response = {}
    html = "auth/loginpage.html"
    return render(request, html, response)

def login (request):
   if request.method == 'POST':
        cursor = connection.cursor()

        username = request.POST['username']
        password = request.POST['password']

        cursor.execute('SELECT username FROM pengguna')
        daftarPengguna =  cursor.fetchall()

        penanda_username = False

        for uname in daftarPengguna :
            if (uname[0] == username):
                penanda_username = True

        cursor.execute('SELECT password FROM pengguna WHERE username = "' + username + '"')
        passwordUser =  cursor.fetchone()[0]

        if (password==passwordUser) and penanda_username :
            request.session['username'] = username
            request.session['is_login'] = "login"
            return HttpResponseRedirect(reverse('login:index'))
        else :
            msg = "Username atau Password Salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('login:index'))

            
        cursor.close()
        return HttpResponseRedirect(reverse('login:index'))

def logout (request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login:index'))
